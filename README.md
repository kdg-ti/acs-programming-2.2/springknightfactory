#  Knights demo
This demo runs Knight and Quest classes in several configurations
All classes are under org.demo.knight
## Plain Classes
PlainMain1 uses an ordinary Knight and Errand classes
Run using IDE or `gradlew plain`

## Abstract Factory
FactoryMain2 uses abstract factory factory.BeanFactory implementation MedievalFactory
Run using IDE or `gradlew factory`

## Spring framework
SpringMain3 uses the Spring framework
Run using IDE or `gradlew bootRun`




