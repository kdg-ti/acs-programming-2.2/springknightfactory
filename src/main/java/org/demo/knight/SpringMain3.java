package org.demo.knight;

import org.demo.knight.spring.config.HeroConfig;
import org.demo.knight.spring.knights.Hero;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SpringMain3 {

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(HeroConfig.class,args);
		context.getBean(Hero.class).embarkOnMission();
		context.close();	}
}
