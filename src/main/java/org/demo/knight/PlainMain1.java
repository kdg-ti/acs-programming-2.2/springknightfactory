package org.demo.knight;


import org.demo.knight.spring.knights.*;

public class PlainMain1 {

	public static void main(String[] args) {
		// LadiesKnight can only do one mission, the class depends on the concrete mission class (RescueDamselQuest)
		new LadiesKnight().embarkOnMission();
		// The handyknight does not depend on the mission. The mission is injected into the constructor
		// The Handyknight can do any mission, it only depends on the interface
		new HandyKnight(new ErrandQuest()).embarkOnMission();
		// Our main method calls the  constructors for our Hero's
		// The constructors depend on the concrete LadiesKnight and HandyKnight class
		// In Main2 we will use a creator pattern to remove this dependency: the abstract factory.
	}
}
