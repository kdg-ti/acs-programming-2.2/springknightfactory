package org.demo.knight;


import org.demo.knight.factory.BeanFactory;
import org.demo.knight.factory.MedievalFactory;
import org.demo.knight.spring.knights.Hero;

public class FactoryMain2 {

	public static void main(String[] args) {
		BeanFactory context = new MedievalFactory();
		Hero johan = context.hero("handy");
		johan.embarkOnMission();

	}
}
