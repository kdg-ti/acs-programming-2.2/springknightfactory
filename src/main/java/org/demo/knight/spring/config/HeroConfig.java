package org.demo.knight.spring.config;

import org.demo.knight.spring.knights.HandyKnight;
import org.demo.knight.spring.knights.Hero;
import org.demo.knight.spring.knights.Mission;
import org.demo.knight.spring.knights.ErrandQuest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HeroConfig {
    @Bean
    public Hero hero(Mission quest) {
        return new HandyKnight(quest);
    }

    @Bean
    public Mission quest() {
        return new ErrandQuest();
    }
}
