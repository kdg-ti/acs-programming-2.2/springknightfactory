package org.demo.knight.spring.knights;


public class HandyKnight implements Hero {
	private Mission quest;

	public HandyKnight(Mission quest) {
		this.quest = quest;
	}

	@Override
	public void embarkOnMission() {
		quest.embark();
	}
}


