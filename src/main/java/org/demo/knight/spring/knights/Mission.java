package org.demo.knight.spring.knights;


public interface Mission {
    void embark();
}
