package org.demo.knight.spring.knights;


public class ErrandQuest implements Mission {

    public void embark() {
        System.out.println("==Quest== Destroy this message after reading...");
    }
}
