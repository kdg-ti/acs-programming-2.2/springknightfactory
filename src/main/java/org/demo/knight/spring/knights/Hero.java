package org.demo.knight.spring.knights;


public interface Hero {
    void embarkOnMission();
}
