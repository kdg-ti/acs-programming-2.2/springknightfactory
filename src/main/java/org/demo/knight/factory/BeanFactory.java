package org.demo.knight.factory;

import org.demo.knight.spring.knights.Hero;

/**
 * @author Jan de Rijke.
 */
public interface BeanFactory {
	Hero hero(String type);
}
